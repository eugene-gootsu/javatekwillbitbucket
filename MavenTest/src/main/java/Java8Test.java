import clients.Client;
//import com.sun.xml.internal.ws.runtime.config.MetroConfig;
import product.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Java8Test {

    public static void main(String[] args) {

        Client c = new Client(001, "Metro");
        List<Product> productList = new ArrayList<>();

        productList.add(new Product(1, "Apple", 5.50));
        productList.add(new Product(2, "Tomato", 7.50));
        productList.add(new Product(3, "Orange", 11.50));
        productList.add(new Product(4, "Banana", 9.50));


        Set<String> filteredList = productList.stream().
                filter(product -> product.getPrice() > 6.00).
                map((productFiltered) -> productFiltered.getName()).collect(Collectors.toSet());

        System.out.println("Products with price >10$ : " + filteredList);


//        anonymous


    }


}
