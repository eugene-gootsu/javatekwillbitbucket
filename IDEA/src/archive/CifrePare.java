package archive;

public class CifrePare {

    public static void main(String[] args) {
        int n = 20;

        System.out.println("Cifre pare de la 0 pana la " + n);

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0)
                System.out.println(i);
        }

    }

}
