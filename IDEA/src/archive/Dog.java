package archive;

public class Dog {

    String name = "archive.Dog";
    String race = "Taxa";
    int age = 5;

    void metoda1(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("gav");
        }
    }

    int metoda2(int i, int j) {
        int summ = i + j;
        return summ;
    }

    @Override
    public String toString() {
        return name + "\n" + race + "\n" + age;
    }
}
