package archive;


public class Model_1 {

    public String name;
    String address = "Chisinau, Tekwill";


    public void metoda1_1(int x) {
        int resultMetoda1_1 = x * x;
        System.out.println("result metoda1_1 = " + resultMetoda1_1);
    }


    int metoda1_2(int y) {
        y = y + 10;
        return y;
    }


    public String metoda1_3(String name) {
        return name + "\nAddress: " + address;
    }


    private void metodaPrivate1 (){
        System.out.println("Hello");
    }
    private void metodaPrivate2 (){
        System.out.println("World!");
    }

    public void callPrivate (){
        System.out.println("result callPrivate :");
        metodaPrivate1();
        metodaPrivate2();
    }


}
