package archive;

public class LedLamp {

    int powerWatt = 10;
    int fluxLumen = 200;
    int lumPerWatt = fluxLumen / powerWatt;
    byte criRa; //symbol _ ?
    int diameterMM;
    int lengthMM;
    int lifespanHours;
    short voltageVolts;
    int itemCode;
    int barcodeEAN13;
    float price = 19.99F; //add literal F
    String model = "Lumineco";
    String type;
    String lampBase;
    boolean dimmable;  //false default
    char quality = 'A';


    public String toString() {
        return powerWatt + "  " + model;

    }


}
