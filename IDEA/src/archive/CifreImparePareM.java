package archive;

public class CifreImparePareM {

    int n, i;

    void cifreImpareMetoda(int n) {
        System.out.println("Cifre impare: ");
        for (int i = 1; i < n; i++) {
            if (i % 2 > 0)
                System.out.println(i);
        }
        System.out.println();
    }

    void cifrePareMetoda(int n) {
        System.out.println("Cifre pare: ");
        for (int i = 1; i < n; i++) {
            if (i % 2 == 0)
                System.out.println(i);
        }
        System.out.println();
    }

    void helloWorld10(int n) {
        for (int i = 1; i <= n; i++) {
                System.out.println("Hello World! ");
        }
    }


}