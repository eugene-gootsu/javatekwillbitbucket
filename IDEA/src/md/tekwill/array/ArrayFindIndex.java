package md.tekwill.array;

public class ArrayFindIndex {

    public int myArr[] = new int[]{00, 11, 22, 11, 33};

    public int findIndex() {

        int index = 0;
        int value = 33;

        for (int i = 0; i < myArr.length; i++) {
            if (myArr[i] == value) {
                index = i;
            }
        }
        return index;
    }


    public int findDuplicateArray() {
        int duplicate = 0;

        for (int i = 0; i < myArr.length; i++) {
            int x = myArr[i];

            for (int y = 1; y < myArr.length; y++) {
                if (x == myArr[y]) duplicate = x;
            }
        }
        return duplicate;

    }


}
