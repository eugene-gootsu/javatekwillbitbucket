package md.tekwill.string;

public class MethodsTest extends StringMethods {


    public int method1(int z) {
        int result = z + this.method2(0) + this.method3(5);
        return result;
    }

    public int method2(int x) {
        x = x + 25;
        return x;
    }

    public int method3(int x) {
        x = x + 25;
        return x;
    }


    public int method2(int x, int y) {
        return x + y;
    }

    public long method2(long x) {
        return x;
    }

    public String m2(String s1, String s2) {
        String s3 = s1 + s2 + "hello";
        return s3;
    }


}
