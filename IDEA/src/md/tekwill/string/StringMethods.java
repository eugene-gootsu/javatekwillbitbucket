package md.tekwill.string;

import java.lang.String;

public class StringMethods {


    public String mytoUpperCase(String s1) {
        return s1.toUpperCase();
    }

    public String mytoLowerCase(String s1) {
        return s1.toLowerCase();
    }

    public int myStringlength(String s1) {
        return s1.length();
    }

    public boolean myEquals(String s1, String s2) {
        return s1.equals(s2);
    }


    public String m2(String s1, String s2) {
        String s3 = s1 + s2;
        return s3;
    }

    //m2 overloading
    public String m2(String s1) {
        String s2 = s1 + "blabla";
        return s2;
    }


}
