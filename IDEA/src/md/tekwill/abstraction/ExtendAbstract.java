package md.tekwill.abstraction;

public class ExtendAbstract extends AbstractClass
        implements MyInterface, MyInterface2 {


    public void abstractMethodOne(){
        System.out.println("Abstract method nr.1 implementation");
    }

    public void abstractMethodTwo(){
        abstractMethodOne();
        System.out.println("Abstract method nr.2 implementation");
    }

    public void interfaceMethodOne(){
        System.out.println("Hello interface!");
    }

}



