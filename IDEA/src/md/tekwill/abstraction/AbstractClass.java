package md.tekwill.abstraction;

public abstract class AbstractClass {


    public abstract void abstractMethodOne();

    public abstract void abstractMethodTwo();

}
