package md.tekwill.abstraction;

public interface MyInterface3 extends MyInterface, MyInterface2 {

    void myInterfaceThreeMethod();

    @Override
    void interfaceMethodOne();

}
