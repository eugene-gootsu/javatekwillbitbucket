package md.tekwill.polymorphism;

public class Test {


    public static void main(String[] args) {


        Child child = new Child();

      child.methodParent();



           Child myChildClass = new Child();
           Child myChildClass2 = new Child();

           Parent parent = new Parent();

           parent = (Parent) myChildClass; //casting ref of child to parent

        System.out.println(parent.equals(myChildClass));

        System.out.println(parent==myChildClass);
        System.out.println(parent==myChildClass2);

        parent = myChildClass2;

        System.out.println(parent==myChildClass2);

        System.out.println("parent vs mychild2 :"+parent.equals(myChildClass2));


    }
}
