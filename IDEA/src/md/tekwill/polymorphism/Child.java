package md.tekwill.polymorphism;

public class Child extends Parent {


    void methodChild(){


        System.out.println("Hello, I am method of subclass");
    }

    void methodParent(){

        super.methodParent();

        System.out.println("Child method");

    }




}
