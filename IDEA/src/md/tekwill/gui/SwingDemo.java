package md.tekwill.gui;
import javax.swing.*;

    public class SwingDemo {

        SwingDemo(){
            JFrame jfrm = new JFrame("A simple swing app");
            jfrm.setSize(275, 100);
            jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            JLabel jlab = new JLabel ("hello swing");
            jfrm.add(jlab);
            jfrm.setVisible(true);

            JButton jbuttonAddPower = new JButton("power");
            jbuttonAddPower.setSize(50,50);

            jfrm.add(jbuttonAddPower);



        }
        public static void main (String args []){
            SwingUtilities.invokeLater(new Runnable(){
                public void run (){

                    new SwingDemo();
                }
            });
        }
    }


