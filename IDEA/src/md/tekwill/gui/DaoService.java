package md.tekwill.gui;

public class DaoService {

    DaoArray dao = new DaoArray();
    LEDLamp ledLamp = new LEDLamp();


    public int getIndexOfItemByCode(int itemCode) {
        int indexItem = 0;

        for (int i = 0; i < dao.ledLampArray.length; i++) {
            if (dao.ledLampArray[i].itemCode == itemCode)
                indexItem = i;

        }
        return indexItem;
    }

    public String foundItemtoString(int indexItem) {

        return dao.ledLampArray[indexItem].toString();
    }

    public void setRetailPrice(double newPrice, int indexItem){

        dao.ledLampArray[indexItem].retailPrice = newPrice;

    }




}
