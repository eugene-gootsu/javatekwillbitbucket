package md.tekwill.exception;

import java.io.FileNotFoundException;
import java.sql.SQLOutput;

public class TestExceptions {


    public static void main(String[] args) {

        try {
            System.out.println("try");
            int x = 5 / 0;
        } catch (ArithmeticException a) {
            System.out.println("catch");
        } finally {
            System.out.println("finally");
        }
        ExceptionMethods excMet = new ExceptionMethods();

        try {
            excMet.read(17);
        } catch (FileNotFoundException b) {
            System.out.println("<18 years!");
        }

        try{
            excMet.update(14);
        }
        catch (RuntimeException runEx){}


        try{
           excMet.testCustomException(15);
        }
        catch (CustomException customEx){

            System.out.println(customEx);
        }


    }

}


