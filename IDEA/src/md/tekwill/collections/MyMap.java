package md.tekwill.collections;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class MyMap {

    public static void main(String[] args) {
        Map<Integer, String> myMap = new HashMap<>();

        myMap.put(1, "one");
        myMap.put(2, "two");
        myMap.put(3, "three");
        myMap.put(4, "four");
        myMap.put(5, "five");

        for (Map.Entry<Integer, String> entryMap : myMap.entrySet()) {

            System.out.println(entryMap.getKey());
            System.out.println(entryMap.getValue());


        }


    }

}
