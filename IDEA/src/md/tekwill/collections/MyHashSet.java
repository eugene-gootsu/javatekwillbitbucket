package md.tekwill.collections;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MyHashSet {

    public static void main(String[] args) {


        Set<Integer> setStr = new HashSet<>();//duplicates not allowed

        setStr.add(1);
        setStr.add(2);
        setStr.add(3);
        setStr.add(4);
        setStr.add(5);
        setStr.add(6);

        for (Integer intSet:setStr) {

            System.out.print(intSet+"  ");

        }

        System.out.println();

        Set<Integer> setStr2 = new TreeSet<>();

        setStr2.add(1);
        setStr2.add(6);
        setStr2.add(2);
        setStr2.add(4);
        setStr2.add(5);
        setStr2.add(3);
        setStr2.add(33);

        for (Integer intSet2 : setStr2) {

            System.out.print(intSet2+"  ");

        }

    }



}

