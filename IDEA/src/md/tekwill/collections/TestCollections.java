package md.tekwill.collections;

import java.util.*;

public class TestCollections {

    public static void main(String[] args) {


        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(4);
        set1.add(2);
        set1.add(3);
        set1.add(6);
        set1.add(5);
        set1.add(5);

        System.out.println(set1);

        List<Integer> list1 = new ArrayList<>();
        list1.add(9);
        list1.add(6);
        list1.add(7);
        list1.add(8);
        list1.add(5);
        list1.add(4);



        for (int i = 0; i < 20; i++) {
            list1.add(i);

        }
        System.out.println(set1);
        set1.addAll(list1);

        set1.add(15);
        set1.add(11);
        set1.add(10);

        System.out.println(set1);
        System.out.println(list1);

        list1.sort(Comparator.naturalOrder());

        System.out.println(list1);

    }

}
