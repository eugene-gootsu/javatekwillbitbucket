package md.tekwill.inheritance;
import md.tekwill.ptotected.Animal;

public class Bird extends Animal {

    public Bird (String type, String habitat, int numberSpecies) {
        super(type, habitat, numberSpecies);
    }

    public void move() {
        System.out.println("flying");
    }

    public void eat(){

        super.eat();
        System.out.println("eating with beak");
    }

    public void nesting() {
        System.out.println("moving");
    }

    public String toString (){

        return type + " " + habitat + " "  + numberSpecies;
    }



}
