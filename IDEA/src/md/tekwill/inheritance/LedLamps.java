package md.tekwill.inheritance;

public class LedLamps {

    public int itemCode;
    public String model;
    public int powerWatt;
    public int fluxLumen;
    public String lampBase;
    public int beamAngle;
    public boolean inStock;
    public int retailPrice;
    public int discount;

    private int cost;
    private int profit;


    public LedLamps(int itemCode, String model, int retailPrice, boolean inStock) {
        this.itemCode = itemCode;
        this.model = model;
        this.retailPrice = retailPrice;
        this.inStock = inStock;

    }

    public int getCost() {
        return this.cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


    public int calcProfitPercent(int retailPrice) {
        return retailPrice / cost - 1;
    }

    public static int convertLedPowerToIncandescent(int powerLed, int beamAngle) {

        int powerIncandescent;
        System.out.print("Incandescent equivalent is : ");

        if (beamAngle > 90) {
            powerIncandescent = powerLed * 80 / 13;
        } else powerIncandescent = powerLed * 80 / 8;

        return powerIncandescent;
    }

    public static int methodThree(int x, int y) {

        return x + y;
    }


    public String toString() {

        return "item code : " + itemCode + "\nmodel : " + model + "\nretail price : "
                + retailPrice + "\nin stock : " + inStock;
    }


}
