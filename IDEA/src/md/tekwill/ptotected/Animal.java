package md.tekwill.ptotected;

  public class Animal {


      public String type;
      public String habitat;
      public int numberSpecies;

      public Animal(String type, String habitat, int numberSpecies) {
          this.type = type;
          this.habitat = habitat;
          this.numberSpecies = numberSpecies;
      }

    public void breathe() {
        
        System.out.println("breathing");
    }

    public void eat() {
        System.out.println("eating");
    }

    public void move() {
        System.out.println("moving");
    }

    public void reproduce() {
        System.out.println("making sex");
    }

}
