/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package md.tekwill.dao;

import md.tekwill.models.Employee;

/**
 *
 * @author adragutan
 */
public class EmployeeDao {

    public static Employee employeeList[] = new Employee[10];

    public boolean create(Employee employee) {
        if (employee != null) {
            for (int i = 0; i < employeeList.length; i++) {
                if (employeeList[i] == null) {
                    employeeList[i] = employee;
                    break;
                }

            }

            return true;
        }

        return false;
    }

    public Employee findById(int id) {

        for (int i = 0; i < employeeList.length; i++) {

            if (id == employeeList[i].getId()) {
                
            return employeeList[i];
            
            }

        }

        return null;
    }

    public void findAll() {

        for (Employee ref : employeeList) {

            if (ref != null) {
                System.out.println(ref);

            }

            //public update
            //delete
        }
    }
}
