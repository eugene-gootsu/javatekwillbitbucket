/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package md.tekwill.models;

/**
 *
 * @author adragutan
 */
public class Employee {
    private int id;
    private String name;
    private int age;
   
    public Employee(){}
    
    
    public Employee(int id, String name, int age){
        this.id=id;
        this.name=name;
        this.age=age;
    
    
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
     public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
     public void setAge(int age) {
        this.age = age;
    }
    
    public int getAge() {
        return this.age;
    }
    
    @Override
    public String toString() {
        return "Id: " + this.id + " Name: " + this.name + " Age: " + this.age;
    }
    
}
