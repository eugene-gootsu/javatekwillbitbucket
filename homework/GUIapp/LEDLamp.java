package LEDLampsDB;

/**
 * @author Eugene
 */

public class LEDLamp {
    int itemCode;
    String type;
    String model;

    int voltage;
    int power;
    String base;
    String colorTemperature;
    int diameter;
    int length;
    boolean dimmable;
    double retailPrice;

    LEDLamp(){

    }
    
    LEDLamp(int itemCode, String type, String model, int voltage, int power, String base,
            String colorTemperature, int diameter, int length, boolean dimmable, double retailPrice) {
        
        this.itemCode = itemCode;
        this.type = type;
        this.model = model;
        this.voltage = voltage;
        this.power = power;
        this.base = base;
        this.colorTemperature = colorTemperature;
        this.diameter = diameter;
        this.length = length;
        this.dimmable = dimmable;
        this.retailPrice = retailPrice;
    }

    @Override
    public String toString() {
        return "\nItem code: " + itemCode + 
               "\nType: " + type +
               "\nInput voltage (V): " + voltage +
               "\nModel: " + model +
               "\nPower (W): " + power + 
               "\nLamp base: " + base +
               "\nColor temperature: " + colorTemperature + 
               "\nLamp diameter (mm): " + diameter +
               "\nLamp length (mm): " + length + 
               "\nDimmable: " + dimmable +
               "\n\nRetail price: " + retailPrice+"\n_________________";

    }
    
    public void setRetailPrice(double newPrice){
        
        this.retailPrice = newPrice;
    
    }
    
    
}
