package LEDLampsDB;

/**
 *
 * @author Eugene
 */
public class DaoArray {


    LEDLamp a10107001 = new LEDLamp(10107001, "omni", "PRO A60", 220, 10,
            "E27", "3000K", 60, 100, false, 45.00);

    LEDLamp a10107002 = new LEDLamp(10107002, "omni", "PRO A65", 220, 12,
            "E27", "3000K", 65, 100, true, 55.00);

    LEDLamp a10107003 = new LEDLamp(10107003, "omni", "PRO A70", 220, 20,
            "E27", "4000K", 70, 120, false, 69.99);


    LEDLamp ledLampArray[] = new LEDLamp[]{a10107001, a10107002, a10107003};


}
