package dao;

import java.util.List;
import java.util.ArrayList;
import models.Employee;
import service.ServiceArrayList;

public class DaoArrayList implements ServiceArrayList {

    List<Employee> employeeArrayList = new ArrayList<>();

    @Override
    public void createEmp(Employee employeeNew) {
        if (employeeNew != null) {
            employeeArrayList.add(employeeNew);
        }
    }

    @Override
    public Employee findEmpById(int id) {
        for (Employee ref : employeeArrayList) {
            if (ref.getId() == id) {
                return ref;
            }
        }
        return null;
    }

    @Override
    public String updateEmp(Employee empUpdate) {
        for (Employee ref : employeeArrayList) {
            if (empUpdate.getId() == ref.getId()) {
                ref.setName(empUpdate.getName());
                ref.setAge(empUpdate.getAge());
                return "OK, updated ";
            }
        }
        return "Not found employee with... ";
    }

    @Override
    public String deleteEmp(int id) {
        for (Employee ref : employeeArrayList) {
            if (id == ref.getId()) {
                employeeArrayList.remove(ref);
                return "OK, deleted ";
            }
        }
        return "Not found ID... ";
    }

    @Override
    public String toString() {
        String result = "";
        for (Employee ref : employeeArrayList) {
            result += ref.toString() + "\n";
        }
        return result;
    }

    @Override
    public String viewEmpAll() {
        if (!toString().isEmpty()) {
            return toString();
        }
        return "Not found any employee...";
    }

}
