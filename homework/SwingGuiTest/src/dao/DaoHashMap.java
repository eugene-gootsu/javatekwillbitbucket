package dao;

import java.util.HashMap;
import java.util.Map;
import models.Employee;
import service.ServiceHashMap;

public class DaoHashMap implements ServiceHashMap {

    Map<Integer, Employee> employeeHashMap = new HashMap<>();

    @Override
    public void createEmp(Employee employeeNew) {
        if (employeeNew != null) {
            employeeHashMap.put(employeeNew.getId(), employeeNew);
        }
    }

    @Override
    public Employee findEmpById(int id) {
        for (Map.Entry <Integer, Employee> ref : employeeHashMap.entrySet()) {
            if (ref.getKey() == id) {
                return ref.getValue();
            }
        }
        return null;
    }

    @Override
    public String updateEmp(Employee empUpdate) {
        for (Map.Entry <Integer, Employee> ref : employeeHashMap.entrySet()) {
            if (ref.getKey() == empUpdate.getId()) {
                ref.setValue(empUpdate);
               return "Updated : ";
            }
        }
        return "Not found ID... ";
    }
    
    @Override
    public String toString() {
        String result = "";
        for (Map.Entry <Integer, Employee> ref : employeeHashMap.entrySet()) {
            result += ref.toString() + "\n";
        }
        return result;
    }

    @Override
    public String viewEmpAll() {
        if (!toString().isEmpty()) {
            return toString();
        }
        return "Not found any employee...";
        
    }

    @Override
    public String deleteEmp(int id) {
        for (Map.Entry <Integer, Employee> ref : employeeHashMap.entrySet()) {
             if (ref.getKey() == id) {
                employeeHashMap.remove(ref.getKey(),ref.getValue());           
                return "Deleted ID : ";
            }
        }
        return "Not found ID... ";
    }

}
