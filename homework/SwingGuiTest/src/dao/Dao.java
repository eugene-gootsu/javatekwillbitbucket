package dao;

import models.Employee;
import service.Service;

public class Dao implements Service {

    public static Employee[] employeeArray = new Employee[10];

    public void createEmp(Employee employee) {
        if (employee != null) {
            for (Employee ref : employeeArray) {
                if (ref == null) {
                    ref = employee;
                    System.out.println(ref);
                    break;
                }
            }
        }
    }

    public Employee findEmpById(int id) {
        for (Employee ref : employeeArray) {
            if (id == ref.getId()) {
                System.out.println(ref);
                return ref;
            }
        }
        return null;
    }

    public void updateEmp(Employee empUpdate) {
        for (Employee ref : employeeArray) {
            if (ref.getId() == empUpdate.getId()) {
                System.out.println(ref);
                ref.setName(empUpdate.getName());
                ref.setAge(empUpdate.getAge());
                break;
            }
        }
    }

    public void deleteEmp(int id) {
        for (Employee ref : employeeArray) {
//            if (ref != null) {
                if (id == ref.getId()) {
                    System.out.println(ref);
                    ref = null;
                    break;
                }
 //           }
        }
    }

    public String viewEmpAll() {
        for (Employee ref : employeeArray) {
            return " ID: " + ref.getId() + "    Name: " + ref.getName()
                    + "    Age: " + ref.getAge();
        }
        return "Nobody found";
    }

}
