
package service;

import java.util.List;
import models.Employee;


public interface ServiceArrayList {
    
    void createEmp (Employee employeeNew);
    
    Employee findEmpById(int id);
    
    String updateEmp(Employee empUpdate);
    
    String viewEmpAll();
    
    String deleteEmp(int id);
    
    
}
