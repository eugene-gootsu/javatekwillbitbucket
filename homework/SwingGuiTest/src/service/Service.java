
package service;
import models.Employee;

public interface Service {
    
    void createEmp (Employee employee);
    
    Employee findEmpById(int id);
    
    void updateEmp(Employee empUpdate);
    
    String viewEmpAll();
    
    void deleteEmp(int id);

}
