package service;

import models.Employee;

public interface ServiceHashMap {

    void createEmp(Employee employeeNew);

    Employee findEmpById(int id);

    String updateEmp(Employee empUpdate);

    String viewEmpAll();

    String deleteEmp(int id);

}
