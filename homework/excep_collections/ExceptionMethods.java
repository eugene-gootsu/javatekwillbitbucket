package md.tekwill.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionMethods {

    public void read(int age) throws FileNotFoundException {
        if(age<18) throw new FileNotFoundException();}

    public void testCustomException (int age) throws CustomException {
        if (age <18) throw new CustomException("custom Ex < 18 years!");

    }

    public void update(int age) throws RuntimeException {
        //??? I dont know how to create a runtime exception
        }









}
