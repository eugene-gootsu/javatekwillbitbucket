import md.tekwill.package1.GetSetTest;
import md.tekwill.package1.Model_1;
import md.tekwill.package2.Model_2;
import md.tekwill.package3.Model_3;

public class TestMain {

    public static void main(String[] args) {

        Model_1 model1 = new Model_1();
        model1.metoda1_1(5);

        System.out.println();

        String resultMetoda_1_3 = model1.metoda1_3("Eugene");
        System.out.println("result metoda1_3 = " + resultMetoda_1_3);

        System.out.println();

        Model_2 model2 = new Model_2();
        Model_3 model3 = new Model_3();
        int y = model3.metoda2(5);
        System.out.println("result override metoda2 y = " + y);

        System.out.println();


        model1.callPrivate();

        System.out.println();
        System.out.println("Testing Get and Set methods");

        GetSetTest getSetTest = new GetSetTest();
        System.out.println(getSetTest.getName());
        System.out.println(getSetTest.getAge());
        getSetTest.setName("Andrei");
        getSetTest.setAge(30);

        System.out.println(getSetTest.getName());
        System.out.println(getSetTest.getAge());












    }
}
