package md.tekwill.abstraction;

public interface MyInterface2 extends MyInterface {


    default void defaultInterfaceMethod() {
        System.out.println("I am default interface method");
    }

    static void staticInterfaceMethod() {
        System.out.println("I am static interface method");
    }

}
