package md.tekwill.abstraction;

public interface MyInterface3 extends MyInterface, MyInterface2 {

    @Override
    void interfaceMethodOne();

}
